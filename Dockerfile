FROM python:3.10

WORKDIR /django

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . /django

EXPOSE 8000

CMD ["python3", "./myproject/manage.py", "runserver", "0.0.0.0:8000"]
